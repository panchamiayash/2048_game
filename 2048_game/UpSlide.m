//
//  UpSlide.m
//  2048_game
//
//  Created by Yash Panchamia on 2/5/17.
//  Copyright © 2017 Yash Panchamia. All rights reserved.
//

#import "UpSlide.h"
#import "ViewController.h"
@implementation UpSlide

-(void)movingTilesUp:(NSArray*)tiles
{
    
    int row12, row23, row34, noOfTimes=1;
    
    while(noOfTimes<=4)
    {
        row12=0;
        while(row12<=3)
        {
            if([[[tiles objectAtIndex:row12]text] isEqual:@""])
            {
                [[tiles objectAtIndex:row12] setText:[[tiles objectAtIndex:row12+4]text]];
                [[tiles objectAtIndex:row12+4] setText:@""];
            }
            row12++;
        }
        
        row23=4;
        while(row23<=7)
        {
            if([[[tiles objectAtIndex:row23]text] isEqual:@""])
            {
                [[tiles objectAtIndex:row23] setText:[[tiles objectAtIndex:row23+4]text]];
                [[tiles objectAtIndex:row23+4] setText:@""];
            }
            row23++;
        }
        
        row34=8;
        while(row34<=11)
        {
            if([[[tiles objectAtIndex:row34]text] isEqual:@""])
            {
                [[tiles objectAtIndex:
                  row34] setText:[[tiles objectAtIndex:row34+4]text]];
                [[tiles objectAtIndex:row34+4] setText:@""];
            }
            row34++;
        }
        
        noOfTimes++;
    }
    
}


@end
