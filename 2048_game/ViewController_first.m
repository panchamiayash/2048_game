//
//  ViewController_first.m
//  2048_game
//
//  Created by Yash Panchamia on 2/8/17.
//  Copyright © 2017 Yash Panchamia. All rights reserved.
//

#import "ViewController_first.h"

@interface ViewController_first ()

@end


@implementation ViewController_first

@synthesize backImage;
@synthesize playImage;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    backImage.image= [UIImage imageNamed:@"2048one.png"];
    backImage.contentMode = UIViewContentModeCenter;
    
    playImage.image= [UIImage imageNamed:@"play.png"];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(onTimerEvent:) userInfo:nil repeats:YES];
    // Do any additional setup after loading the view.
}

-(void)onTimerEvent:(NSTimer *) timer
{
        self.playImage.hidden = !self.playImage.hidden;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
