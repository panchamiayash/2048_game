//
//  ViewController.m
//  2048_game
//
//  Created by Yash Panchamia on 2/1/17.
//  Copyright © 2017 Yash Panchamia. All rights reserved.
//

#import "ViewController.h"
#import "LeftSlide.h"
#import "RightSlide.h"
#import "UpSlide.h"
#import "DownSlide.h"
#import <QuartzCore/CALayer.h>
#import "TileColorChange.h"

@interface ViewController ()

@end

@implementation ViewController
int finalScore=0,steps=0;
@synthesize tiles;
@synthesize leftSwipe;
@synthesize rightSwipe;
@synthesize upSwipe;
@synthesize downSwipe;
@synthesize matrixBack;
@synthesize Scoreboard;
@synthesize scoreText;
@synthesize stepsText;
@synthesize pointScored;
@synthesize bottomMSG;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
//    scoreBoard.layer.cornerRadius = 8.0;
//    scoreBoard.layer.borderColor=[[UIColor orangeColor]CGColor];
//    scoreBoard.layer.backgroundColor =[[UIColor yellowColor]CGColor];
//    //scoreBoard.layer.masksToBounds = true;
//    scoreBoard.layer.borderWidth = 2.0;

    
    
    matrixBack.layer.cornerRadius = 8.0;
    Scoreboard.layer.masksToBounds = true;
    Scoreboard.layer.cornerRadius = 30.0;
    Scoreboard.backgroundColor = [UIColor orangeColor];
    Scoreboard.textColor = [UIColor whiteColor];
    Scoreboard.text =@"SCORE\n0";
    
    stepsText.textColor = [UIColor orangeColor];
    stepsText.text =@"#Steps\n0";
    
    
    for(int i=0; i<=15;i++)
    {
        [[[tiles objectAtIndex:i] layer] setCornerRadius:8];
        [[[tiles objectAtIndex:i] layer] setMasksToBounds :YES];
        [[tiles objectAtIndex:i] setBackgroundColor:[UIColor whiteColor]];
        [[tiles objectAtIndex:i] setTextColor:[UIColor whiteColor]];
    }
    
    
    
    //empty the labels initially
    for (int i=0; i<=15; i++)
    {
        [[tiles objectAtIndex:i] setText:@""];
    }

    //randomly generate index of 2 tils and put '2' there
    int rand1 = arc4random() % 15;
    int rand2 = arc4random() % 15;
    [[tiles objectAtIndex:rand1] setText:@"2"];
    [[tiles objectAtIndex:rand1] setBackgroundColor:[UIColor yellowColor]];
    [[tiles objectAtIndex:rand2] setText:@"2"];
    [[tiles objectAtIndex:rand2] setBackgroundColor:[UIColor yellowColor]];
    
    
    //left swipe gesture
    self.leftSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRecog:)];
    self.leftSwipe.direction = UISwipeGestureRecognizerDirectionLeft;
    [[self view] addGestureRecognizer:leftSwipe];
    
    //right swipe
    self.rightSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRecog:)];
    self.rightSwipe.direction = UISwipeGestureRecognizerDirectionRight;
    [[self view] addGestureRecognizer:rightSwipe];
    
    //up swipe
    self.upSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRecog:)];
    self.upSwipe.direction = UISwipeGestureRecognizerDirectionUp;
    [[self view] addGestureRecognizer:upSwipe];
    
    //down swipe
    self.downSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRecog:)];
    self.downSwipe.direction = UISwipeGestureRecognizerDirectionDown;
    [[self view] addGestureRecognizer:downSwipe];
    
}
    
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//======================= SWIPE RECOG FUNCTION ================================

- (void)swipeRecog:(UISwipeGestureRecognizer *)sender
{
    steps++;
    stepsText.text = [NSString stringWithFormat:@"#Steps\n%i",steps];
    TileColorChange *color =[[TileColorChange alloc]init];
    
    //================= LEFT DIRECTION ================
    if (sender.direction == UISwipeGestureRecognizerDirectionLeft)
    {         NSLog(@"left entry");


        // tiles to left, call funct in leftslide.m
        LeftSlide *left =[[LeftSlide alloc]init];
        [left movingTilesLeft:tiles];
        
        //adding columns
        int counter=0;
        while (counter<=2)
        {
            for(int i=counter; i<=12; i+=4)
            {
                if ([[[tiles objectAtIndex:i]text] isEqual:[[tiles objectAtIndex:i+1]text]] && ![[[tiles objectAtIndex:i]text]isEqualToString:@""])
                {
                    int m = [[[tiles objectAtIndex:i] text] intValue];
                    int n = [[[tiles objectAtIndex:i+1] text] intValue];
                    int sum =m+n;
                    finalScore+=sum;
                    Scoreboard.text = [NSString stringWithFormat:@"SCORE\n%i",finalScore];
                    pointScored.text=[NSString stringWithFormat:@"+%i",sum];
                    pointScored.hidden=NO;
                    [self performSelector:@selector(hideLabel) withObject:nil afterDelay:1];//1sec
                    [[tiles objectAtIndex:i] setText: [NSString stringWithFormat:@"%d",sum] ];
                    [[tiles objectAtIndex:i+1] setText:@""];
                    [self printMsg:bottomMSG];

                }
            }
            counter++;
        }//end adding
        
        // tiles to left, call funct in slideleft.m
        [left movingTilesLeft:tiles];
        //[color changecolor:tiles];

    }//end of left swipe
    
    
    //================= RIGHT DIRECTION ================
    if (sender.direction == UISwipeGestureRecognizerDirectionRight)
    {
        NSLog(@"right entry");
        
        RightSlide *right = [[RightSlide alloc]init];
        // tiles to right, call funct in slideright.m
        [right movingTilesRight:tiles];
       
        //adding columns
        int counter=3;
        while (counter<=3 && counter>=1)
        {
            
            for(int i=counter; i<=15; i+=4)
            {
                
                if ([[[tiles objectAtIndex:i]text] isEqual:[[tiles objectAtIndex:i-1]text]] && ![[[tiles objectAtIndex:i]text]isEqualToString:@""])
                {
                    int m = [[[tiles objectAtIndex:i] text] intValue];
                    int n = [[[tiles objectAtIndex:i-1] text] intValue];
                    int sum =m+n;
                    finalScore+=sum;
                    pointScored.text=[NSString stringWithFormat:@"+%i",sum];
                    pointScored.hidden=NO;
                    [self performSelector:@selector(hideLabel) withObject:nil afterDelay:1];
                    Scoreboard.text = [NSString stringWithFormat:@"SCORE\n%i",finalScore];
                    [[tiles objectAtIndex:i] setText: [NSString stringWithFormat:@"%d",sum] ];
                    [[tiles objectAtIndex:i-1] setText:@""];
                    [self printMsg:bottomMSG];

                }
            }
            counter--;
        }//end adding
        
        // tiles to right, call funct in slideright.m
        [right movingTilesRight:tiles];
        //[color changecolor:tiles];

    }//end right swipe.
    
    
    //================= UP DIRECTION ================
    if (sender.direction == UISwipeGestureRecognizerDirectionUp)
    {
        NSLog(@"up entry");
        UpSlide *up=[[UpSlide alloc]init];
        // tiles to up, call funct in slideup.m
        [up movingTilesUp:tiles];
        
        //adding columns
        int counter=0;
        while (counter<=3)
        {
            for(int i=counter; i<=11; i+=4)
            {
                if ([[[tiles objectAtIndex:i]text] isEqual:[[tiles objectAtIndex:i+4]text]] && ![[[tiles objectAtIndex:i]text]isEqualToString:@""])
                {
                    int m = [[[tiles objectAtIndex:i] text] intValue];
                    int n = [[[tiles objectAtIndex:i+4] text] intValue];
                    int sum =m+n;
                    finalScore+=sum;
                    pointScored.text=[NSString stringWithFormat:@"+%i",sum];
                    pointScored.hidden=NO;
                    [self performSelector:@selector(hideLabel) withObject:nil afterDelay:1];
                    Scoreboard.text = [NSString stringWithFormat:@"SCORE\n%i",finalScore];
                    [[tiles objectAtIndex:i] setText: [NSString stringWithFormat:@"%d",sum] ];
                    [[tiles objectAtIndex:i+4] setText:@""];
                    [self printMsg:bottomMSG];

                }
            }
            counter++;
        }//end adding
        
        // tiles to left, call funct in slideleft.m
        [up movingTilesUp:tiles];

    }//end of up swipe

    
    //================= DOWN DIRECTION ================
    if (sender.direction == UISwipeGestureRecognizerDirectionDown)
    {         NSLog(@"down entry");
        
        
        DownSlide *down=[[DownSlide alloc]init];
        // tiles to up, call funct in slideup.m
        [down movingTilesDown:tiles];
        
        //adding columns
        int counterd=12;
        while (counterd<=15)
        {
            for(int i=counterd; i>=4 ; i-=4)
            {
                if ([[[tiles objectAtIndex:i]text] isEqual:[[tiles objectAtIndex:i-4]text]] && ![[[tiles objectAtIndex:i]text]isEqualToString:@""])
                {
                    NSLog(@"here");
                    int m = [[[tiles objectAtIndex:i] text] intValue];
                    int n = [[[tiles objectAtIndex:i-4] text] intValue];
                    int sum =m+n;
                    finalScore+=sum;
                    pointScored.text=[NSString stringWithFormat:@"+%i",sum];
                    pointScored.hidden=NO;
                    [self performSelector:@selector(hideLabel) withObject:nil afterDelay:1];
                    Scoreboard.text = [NSString stringWithFormat:@"SCORE\n%i",finalScore];
                    [[tiles objectAtIndex:i] setText: [NSString stringWithFormat:@"%d",sum] ];
                    [[tiles objectAtIndex:i-4] setText:@""];
                    [self printMsg:bottomMSG];

                }
            }
            counterd++;
        }//end adding
        
        // tiles to left, call funct in slideleft.m
        [down movingTilesDown:tiles];
        
    }//end of down swipe
    
    //generating random tiles.
    [self generateTilesAtRandon];
    //chnage color of each tile.
    [color changecolor:tiles];
    
    
}//end swipeRecof()
    

//=============== Generate random Tile No. =====================
-(void)generateTilesAtRandon
{
    int rand= arc4random()%2;
    int tileValue,tileNo;
    if(rand == 1)
        tileValue = 2;
    else
        tileValue =4;
    
    while(1)
    {
        tileNo = arc4random()%15;
        if(tileNo!=5 && tileNo!=6 && tileNo!=9 && tileNo!=10 && [[[tiles objectAtIndex:tileNo] text ]isEqualToString:@""])
        {
            [[tiles objectAtIndex:tileNo] setText:[NSString stringWithFormat:@"%d", tileValue]];
            break;
        }
    }
}


- (IBAction)restartGame:(id)sender
{
    for (int i=0; i<=15; i++)
    {
        [[tiles objectAtIndex:i] setText:@""];
        [[tiles objectAtIndex:i] setBackgroundColor:[UIColor whiteColor]];

    }
    
    //randomly generate index of 2 tils and put '2' there
    int rand1 = arc4random() % 15;
    int rand2 = arc4random() % 15;
    [[tiles objectAtIndex:rand1] setText:@"2"];
    [[tiles objectAtIndex:rand1] setBackgroundColor:[UIColor yellowColor]];
    [[tiles objectAtIndex:rand2] setText:@"2"];
    [[tiles objectAtIndex:rand2] setBackgroundColor:[UIColor yellowColor]];
    
    steps = 0;
    stepsText.text = [NSString stringWithFormat:@"#Steps\n%i",steps];
    finalScore = 0;
    Scoreboard.text = [NSString stringWithFormat:@"SCORE\n%i",finalScore];
    
}

-(void)hideLabel
{
    pointScored.hidden=YES;
}

-(void) printMsg: (UILabel *) bottomMsg
{
    int a = arc4random()%5;
    switch(a)
    {
        case 0:
            bottomMsg.text = [NSString stringWithFormat:@"WoW...!!!"];
            bottomMsg.hidden=NO;
            [self performSelector:@selector(hideBottomLabel) withObject:nil afterDelay:1];
            break;
            
        case 1:
            bottomMsg.text = [NSString stringWithFormat:@"Thats great !!"];
            bottomMsg.hidden=NO;
            [self performSelector:@selector(hideBottomLabel) withObject:nil afterDelay:1];
            break;
        
        case 2:
            bottomMsg.text = [NSString stringWithFormat:@"You are on FIRE !!"];
            bottomMsg.hidden=NO;
            [self performSelector:@selector(hideBottomLabel) withObject:nil afterDelay:1];
            break;
        
        case 3:
            bottomMsg.text = [NSString stringWithFormat:@"Nailed it !!"];
            bottomMsg.hidden=NO;
            [self performSelector:@selector(hideBottomLabel) withObject:nil afterDelay:1];
            break;
        
        case 4:
            bottomMsg.text = [NSString stringWithFormat:@"Awesome"];
            bottomMsg.hidden=NO;
            [self performSelector:@selector(hideBottomLabel) withObject:nil afterDelay:1];
            break;
            
    }
}

-(void)hideBottomLabel
{
    bottomMSG.hidden=YES;
}


@end
