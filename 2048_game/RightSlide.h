//
//  RightSlide.h
//  2048_game
//
//  Created by Yash Panchamia on 2/5/17.
//  Copyright © 2017 Yash Panchamia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ViewController.h"
@interface RightSlide : NSObject

-(void)movingTilesRight:(NSArray*)tiles;

@end
