//
//  LeftSlide.m
//  2048_game
//
//  Created by Yash Panchamia on 2/5/17.
//  Copyright © 2017 Yash Panchamia. All rights reserved.
//

#import "LeftSlide.h"
#import "ViewController.h"

@implementation LeftSlide

-(void)movingTilesLeft:(NSArray*)tiles
{
    int counter12, counter23, counter34;
    int noOfTimes=1;
    while (noOfTimes<=4)
    {
        counter12=0;
        while(counter12<=12)
        {
            //NSLog(@"12");
            if([[[tiles objectAtIndex:counter12]text] isEqual:@""])
            {
                [[tiles objectAtIndex:counter12] setText:[[tiles objectAtIndex:counter12+1]text]];
                [[tiles objectAtIndex:counter12+1] setText:@""];
            }
            counter12+=4;
        }
        
        counter23=1;
        while(counter23<=13)
        {
            //NSLog(@"23");
            if([[[tiles objectAtIndex:counter23]text] isEqual:@""])
            {
                [[tiles objectAtIndex:counter23] setText:[[tiles objectAtIndex:counter23+1]text]];
                [[tiles objectAtIndex:counter23+1] setText:@""];
            }
            counter23+=4;
        }
        
        counter34=2;
        while(counter34<=14)
        {
            //NSLog(@"34");
            if([[[tiles objectAtIndex:counter34]text] isEqual:@""])
            {
                [[tiles objectAtIndex:counter34] setText:[[tiles objectAtIndex:counter34+1]text]];
                [[tiles objectAtIndex:counter34+1] setText:@""];
            }
            counter34+=4;
        }
        
        noOfTimes++;
    }
    
}


@end
