//
//  RightSlide.m
//  2048_game
//
//  Created by Yash Panchamia on 2/5/17.
//  Copyright © 2017 Yash Panchamia. All rights reserved.
//

#import "RightSlide.h"

@implementation RightSlide

-(void)movingTilesRight:(NSArray*)tiles
{
    int counter43, counter32, counter21;
    int noOfTimes=1;
    while(noOfTimes<=4)
    {
        counter43=3;
        while(counter43<=15)
        {
            if([[[tiles objectAtIndex:counter43]text] isEqual:@""])
            {
                [[tiles objectAtIndex:counter43] setText:[[tiles objectAtIndex:counter43-1]text]];
                [[tiles objectAtIndex:counter43-1] setText:@""];
            }
            counter43+=4;
        }
        
        counter32=2;
        while (counter32<=14)
        {
            if([[[tiles objectAtIndex:counter32]text] isEqual:@""])
            {
                [[tiles objectAtIndex:counter32] setText:[[tiles objectAtIndex:counter32-1]text]];
                [[tiles objectAtIndex:counter32-1] setText:@""];
            }
            counter32+=4;
        }
        
        counter21=1;
        while(counter21<=13)
        {
            if([[[tiles objectAtIndex:counter21]text] isEqual:@""])
            {
                [[tiles objectAtIndex:counter21] setText:[[tiles objectAtIndex:counter21-1]text]];
                [[tiles objectAtIndex:counter21-1] setText:@""];
            }
            counter21+=4;
        }
        
        noOfTimes++;
    }
}

@end
