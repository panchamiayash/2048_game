//
//  DownSlide.m
//  2048_game
//
//  Created by Yash Panchamia on 2/5/17.
//  Copyright © 2017 Yash Panchamia. All rights reserved.
//

#import "DownSlide.h"

@implementation DownSlide
-(void)movingTilesDown:(NSArray*)tiles
{
    int row43, row32, row21, noOfTimes=1;
    
    while(noOfTimes<=4)
    {
        row43=12;
        while(row43<=15)
        {
            if([[[tiles objectAtIndex:row43]text] isEqual:@""])
            {
                [[tiles objectAtIndex:row43] setText:[[tiles objectAtIndex:row43-4]text]];
                [[tiles objectAtIndex:row43-4] setText:@""];
            }
            row43++;
        }
        
        row32=8;
        while(row32<=11)
        {
            if([[[tiles objectAtIndex:row32]text] isEqual:@""])
            {
                [[tiles objectAtIndex:row32] setText:[[tiles objectAtIndex:row32-4]text]];
                [[tiles objectAtIndex:row32-4] setText:@""];
            }
            row32++;
        }
        
        row21=4;
        while(row21<=7)
        {
            if([[[tiles objectAtIndex:row21]text] isEqual:@""])
            {
                [[tiles objectAtIndex:row21] setText:[[tiles objectAtIndex:row21-4]text]];
                [[tiles objectAtIndex:row21-4] setText:@""];
            }
            row21++;
        }
        noOfTimes++;
        
    }
}
@end
