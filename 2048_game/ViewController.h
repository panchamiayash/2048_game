//
//  ViewController.h
//  2048_game
//
//  Created by Yash Panchamia on 2/1/17.
//  Copyright © 2017 Yash Panchamia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *tiles;

@property (nonatomic, strong) UISwipeGestureRecognizer *leftSwipe;

@property (nonatomic, strong) UISwipeGestureRecognizer *rightSwipe;

@property (nonatomic, strong) UISwipeGestureRecognizer *upSwipe;

@property (nonatomic, strong) UISwipeGestureRecognizer *downSwipe;

@property (weak, nonatomic) IBOutlet UILabel *Scoreboard;

@property (weak, nonatomic) IBOutlet UIView *matrixBack;

@property (weak, nonatomic) IBOutlet UIButton *resetButton;

@property (weak, nonatomic) IBOutlet UILabel *scoreText;

@property (weak, nonatomic) IBOutlet UILabel *stepsText;

@property (weak, nonatomic) IBOutlet UILabel *pointScored;

@property (weak, nonatomic) IBOutlet UIImageView *gamebackground;
@property (weak, nonatomic) IBOutlet UILabel *bottomMSG;

- (IBAction)restartGame:(id)sender;

@end

