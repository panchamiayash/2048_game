//
//  TileColorChange.m
//  2048_game
//
//  Created by Yash Panchamia on 2/8/17.
//  Copyright © 2017 Yash Panchamia. All rights reserved.
//

#import "TileColorChange.h"
#import "ViewController.h"
@implementation TileColorChange

-(void)changecolor: (NSArray *) tiles
{
    for(int i=0; i<=15; i++)
    {
        if([[tiles objectAtIndex:i] isEqual:@""])
        {
            [[tiles objectAtIndex:i] setBackgroundColor:[UIColor whiteColor]];
            //NSLog(@"here");
        }
        
        else
        {
            int m = [[[tiles objectAtIndex:i] text] intValue];
           // NSLog(@"value of n=%i",m);
            switch(m)
            {
                case 0:
                    [[tiles objectAtIndex:i] setBackgroundColor:[UIColor whiteColor]];
                    break;
                    
                case 2:
                    [[tiles objectAtIndex:i] setBackgroundColor:[UIColor yellowColor]];
                    break;
                    
                case 8:
                    [[tiles objectAtIndex:i] setBackgroundColor:[UIColor colorWithRed:230.0f/255.0f green:145.0f/255.0f blue:56.0f/255.0f alpha:1.0]];
                    break;
                    
                case 4:
                    [[tiles objectAtIndex:i] setBackgroundColor:[UIColor colorWithRed:241.0f/255.0f green:194.0f/255.0f blue:50.0f/255.0f alpha:1.0]];
                    break;
                    
                case 16:
                    [[tiles objectAtIndex:i] setBackgroundColor:[UIColor colorWithRed:134.0f/255.0f green:75.0f/255.0f blue:15.0f/255.0f alpha:1.0]];
                    break;
                    
                case 32:
                    [[tiles objectAtIndex:i] setBackgroundColor:[UIColor colorWithRed:229.0f/255.0f green:65.0f/255.0f blue:32.0f/255.0f alpha:1.0]];
                    break;
                    
                case 64:
                    [[tiles objectAtIndex:i] setBackgroundColor:[UIColor colorWithRed:119.0f/255.0f green:75.0f/255.0f blue:7.0f/255.0f alpha:1.0]];
                    break;
                    
                case 128:
                    [[tiles objectAtIndex:i] setBackgroundColor:[UIColor colorWithRed:204.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:1.0]];
                    break;
                
                case 256:
                    [[tiles objectAtIndex:i] setBackgroundColor:[UIColor colorWithRed:128.0f/255.0f green:18.0f/255.0f blue:18.0f/255.0f alpha:1.0]];
                    break;
                    
                case 512:
                    [[tiles objectAtIndex:i]setBackgroundColor:[UIColor colorWithRed:14.0f/255.0f green:74.0f/255.0f blue:87.0f/255.0f alpha:1.0]];
                    break;
                    
                case 1024:
                    [[tiles objectAtIndex:i] setBackgroundColor: [UIColor colorWithRed:12.0f/255.0f green:73.0f/255.0f blue:129.0f/255.0f alpha:1.0]];
                    break;
                    
                case 2048:
                    [[tiles objectAtIndex:i] setBackgroundColor:[UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:1.0]];
                    break;

            }
        }
    }
}
@end
