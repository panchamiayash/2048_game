//
//  ViewController_first.h
//  2048_game
//
//  Created by Yash Panchamia on 2/8/17.
//  Copyright © 2017 Yash Panchamia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController_first : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *backImage;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UIImageView *playImage;

@property (nonatomic, strong) NSTimer *timer;

@end
